import grpc
from concurrent import futures
import time
import random
import atexit
import json
from multiprocessing import Process

from utils import parse_parameters_file

import game_chief.gamechief_pb2 as gamechief_pb2
import game_chief.gamechief_pb2_grpc as gamechief_pb2_grpc


MIN_PORT_NUMBER = 50000
MAX_PORT_NUMBER = 60000


class GameChiefServicer(gamechief_pb2_grpc.GameChiefServicer):
    def __init__(self, shared_game_ips_ports, game_chief_port, number_of_games, game_setup_dict, max_workers):
        self._shared_game_ips_ports = shared_game_ips_ports
        self._server_port = game_chief_port
        self._number_of_games = number_of_games
        self._game_setup_dict = game_setup_dict
                
        self._max_workers = max_workers
        self._generate_random_ports()
        # atexit.register(self.close_server)


    def _generate_random_ports(self):
        for i in range(self._number_of_games):
            game_details = {}
            game_details["port"] = str(random.randint(MIN_PORT_NUMBER, MAX_PORT_NUMBER))
            game_details["ip"] = ""
            game_details["taken"] = False
            self._shared_game_ips_ports[i] = game_details
        
        # print("===")
        # print("=== Generated random ports")
        # print(self._shared_game_ips_ports)
        # print("===")

    def _start_server(self):
        self._game_chief_server = grpc.server(futures.ThreadPoolExecutor(max_workers=self._max_workers))
        gamechief_pb2_grpc.add_GameChiefServicer_to_server(self, self._game_chief_server)
        self._game_chief_server.add_insecure_port('[::]:{}'.format(self._server_port))
        self._game_chief_server.start()
        print ('=== Game chief server running at port {} ...'.format(self._server_port))
        try:
            while True:
                time.sleep(60*60*60)
        except KeyboardInterrupt:
            self._game_chief_server.stop(0)
            print('Game chief server Stopped ...')


    def start_server(self):
        process = Process(target=self._start_server)
        process.start()


    # def close_server(self):
    #     print ('=== Closing Game chief server ...')
    #     self._game_chief_server.stop(0)


    def GetPortAndGameSetup(self, request, context):
        remote_game_ip = request.ipAddress
        remote_port = None
        for i in range(self._number_of_games):
            if self._shared_game_ips_ports[i]["ip"] is "":
                remote_port = self._shared_game_ips_ports[i]["port"]
                updated_gNumber of agent's observations: 53, Stacking length: 3, Total observations: 159ame_details = self._shared_game_ips_ports[i]
                updated_game_details["ip"] = remote_game_ip
                self._shared_game_ips_ports[i] = updated_game_details
                break
                
        if remote_port is None:
            raise ValueError(
                "Could not give port to a remote game asking for it")

        remote_game_port_and_setup = gamechief_pb2.RemoteGamePortAndSetup()
        remote_game_port_and_setup.port = remote_port
        remote_game_port_and_setup.setup = json.dumps(self._game_setup_dict)
        print("Remote game's ip: {}. Sending port: {}".format(remote_game_ip, remote_port))
        return remote_game_port_and_setup


# Testing GameChief
if __name__ == '__main__':
    from multiprocessing import Manager
    shared_game_ips_ports = Manager().dict()
    game_chief_port = 40000
    number_of_games = 2
    max_workers = 1

    game_setup_dict = parse_parameters_file("trainer-params.json")

    server = GameChiefServicer(shared_game_ips_ports, game_chief_port, number_of_games, game_setup_dict, max_workers)
    server.start_server()

    try:
        while True:
            time.sleep(60*60*60)
    except KeyboardInterrupt:
        server.close_server()
        print('Game chief server Stopped ...')