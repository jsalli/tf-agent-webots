import os

import numpy as np
from tf_agents.environments import py_environment, utils
from tf_agents.specs import array_spec
from tf_agents.trajectories import time_step
from ai_game import AIGame
import json
import random
import time

class AIEnvironment(py_environment.PyEnvironment):
    def __init__(self, connection, shared_training_status, local_game, game_params_dict):
        super().__init__()
        self._current_level = -1
        self._game_params_dict = game_params_dict
        self._max_level = len(self._game_params_dict["levelParameters"]["thresholds"])
        self._shared_training_status = shared_training_status

        game, batch_size = self.configure_game(connection, local_game, self._game_params_dict)
        self._game = game
        self._batch_size = batch_size
        self._num_actions = self._game.get_num_of_actions()

        self._num_observations = self._game.get_num_of_observations()
        self._stacked_vectors_length = self._game_params_dict["training"]["stackedVectors"]
        self._num_stacked_observations = self._num_observations * self._stacked_vectors_length
        self._observations = np.zeros(self._num_observations)

        self._action_spec = array_spec.BoundedArraySpec(shape=(), dtype=np.int32, minimum=0, maximum=self._num_actions - 1, name='action')
        self._observation_spec = array_spec.BoundedArraySpec(shape=(self._num_stacked_observations,), dtype=np.float64, minimum=0, maximum=1, name='observation')
        
        self._first_observations = True
        print("=======")
        print("Number of agent's observations: {}, Stacking length: {}, Total observations: {}".format(
            self._num_observations, self._stacked_vectors_length, self._num_stacked_observations))
        print("=======")


    # @staticmethod
    def configure_game(self, connection, local_game, game_params_dict):
        game = AIGame(connection, local_game, game_params_dict)
        reset_params = self._create_reset_params(self._game_params_dict, self._shared_training_status, self._current_level)
        batch_size = game.reset(reset_params)
        return game, batch_size

    @property
    def batched(self):
        return False


    def batch_size(self):
        return self._batch_size


    def action_spec(self):
        return self._action_spec


    def observation_spec(self):
        return self._observation_spec


    def _get_agent_observations(self):
        observations = self._game.get_observations()
        if self._first_observations is True:
            self._first_observations = False
            observations = observations * self._stacked_vectors_length
            self._observations = np.array(observations, dtype=np.float64)
            return self._observations

        self._observations = np.roll(self._observations, self._num_observations)
        self._observations[:self._num_observations] = observations
        return self._observations


    def _reset(self):
        reset_params = self._create_reset_params(self._game_params_dict, self._shared_training_status, self._current_level)
        if reset_params is not None:
            self._current_level += 1
        
        self._game.reset(reset_params)
        self._first_observations = True
        return time_step.restart(self._get_agent_observations())


    def _step(self, action):
        if self._game.is_episode_finished():
            # The last action ended the episode. Ignore the current action and start a new episode.
            return self.reset()

        # execute action and receive reward
        reward = self._game.make_action(action)

        # return transition depending on game state
        if self._game.is_episode_finished():
            return time_step.termination(self._get_agent_observations(), reward)
        else:
            return time_step.transition(self._get_agent_observations(), reward)


    def close(self):
        if self._game is not None:
            self._game.close()
    

    def _create_reset_params(self, game_params_dict, training_status, current_level):
        reset_params = None
        if (
            game_params_dict is not None and
            game_params_dict["levelParameters"]["measure"] == "reward"):
            if (
                training_status["AverageReturn"] > game_params_dict["levelParameters"]["thresholds"][current_level] or
                current_level == -1):
                current_level += 1
                if current_level < self._max_level:
                    print("### Loading new level: {}".format(current_level + 1))
                    reset_params = {}
                    for key in game_params_dict["levelParameters"]["parameters"].keys():
                        reset_params[key] = game_params_dict["levelParameters"]["parameters"][key][current_level]
                    # print("### Parameters are: {}".format(reset_params))
        
        return reset_params