# TF Agents trainer for Webots environment
This repo is meant to be used together with [repo](https://gitlab.com/jsalli/artificialinvaders2019) which is made for a RobotUprising Artificial invaders challenge. This repo uses [tf-agents](https://github.com/tensorflow/agents) and is inspired by the following resources [tf-agents HalfCheetah trainer](https://github.com/tensorflow/agents/blob/master/tf_agents/agents/ppo/examples/v2/train_eval.py), article of [tf-agents playing Doom](https://www.arconsis.com/unternehmen/blog/reinforcement-learning-doom-with-tf-agents-and-ppo) and it's code [github repo](https://github.com/arconsis/blog-playing-doom-with-tf-agents)

## Setup a virtual environment
This repo is tested with Python 3.6.8.

Create Python virtual environment if you want.
```
virtualenv trainer_env
```
```
source trainer_env/bin/activate
```

### Install Python packages
```
pip install -r requirements-gpu.txt
```
Use the `requirements-cpu.txt` if you train on CPU.

**Note for Windows:** Download Shapely-package which has it's geos-dependency built in from link below. Choose the one which matches your system from [here](https://www.lfd.uci.edu/~gohlke/pythonlibs/#shapely). Install with `pip install Shapely-1.6.4.post2-cp37-cp37m-win_amd64.whl` or what ever is your system's package name.

## gRPC and protocol buffers
There are two gRPC connections this repo uses to communicate with the Webots environments.
1. gamecommunication_single.proto
    - Game step/reset/observations and other communication with Webots
2. gamechief.proto
    - Send the port to Webots to use for game communication

These `.proto` files have to be the same that are used in the [Artificial invaders repo](https://gitlab.com/jsalli/artificialinvaders2019).

### Building the protocol buffers for the gRPCs
Run the following lines in the root folder of this project.
```
python -m grpc_tools.protoc --proto_path=. --python_out=. --grpc_python_out=. ./game_communication/gamecommunication_single.proto
```
```
python -m grpc_tools.protoc --proto_path=. --python_out=. --grpc_python_out=. ./game_chief/gamechief.proto
```

## Run the training
`trainer-params.json`-file configures some of the things of the training and the environment. You can set the tf-agent trainer to either start local games for training or wait for remote games on other computers on the same network to connect. The number of local, remote and evaluation games can be set in "game"/"local"/"number" and the same fields for "remote" and "evaluation". Check at least that the "pathToExecutable" and the "level_paths"-fields are correct for your system.


The `game_wrapper.py` and `game-wrapper-params.json` can be used in the remote computer to start needed number of remote Webots games which connect to this tf-agents trainer.

Set the `modelPath`-field also to point to the location you want to save the trained model to.

Run the training with:
```
python ai_tf_agent.py --params_file_path=trainer-params.json
```



