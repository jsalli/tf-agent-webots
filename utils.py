import datetime
import sys
import json
import time
import atexit
import traceback
from absl import logging
from multiprocessing import Process, Pipe
from game_communication.game_communication import GameCommunication


_READY = 1
_GET_NUMBER_OF_OBSERVATION = 2
_RESET = 3
_IS_EPISODE_FINISHED = 4
_MAKE_ACTION = 5
_GET_OBSERVATIONS = 6
_CLOSE = 7
_RESULT = 8
_EXCEPTION = 9


def env_gen(env, connection, shared_training_status, local_game, game_env_path):
    return lambda: env(connection, shared_training_status, local_game, game_env_path)


def log_exit_time(start_time):
    stop_time = datetime.datetime.now()
    print("============")
    print("Training started: {}".format(start_time))
    print("============")

    print("============")
    print("Training ended: {}".format(stop_time))
    print("============")

    print("============")
    print("Training duration: {}".format(stop_time - start_time))
    print("============")


def parse_parameters_file(params_file_path):
    parameters = None
    with open(params_file_path, 'r') as file:
        json_string = file.read()
        parameters = json.loads(json_string)
    return parameters


def create_training_envs(num_parallel_training_environments, environment, connections, shared_training_status, local_game, game_env_path):
    if len(connections) is not num_parallel_training_environments:
        raise ValueError("Number of given connections should be the same as the number of environments")
    
    envs = []
    for i in range(num_parallel_training_environments):
        envs.append(env_gen(environment, connections[i], shared_training_status, local_game[i], game_env_path))
    return envs


def start_clients(conn, shared_game_ips_ports):
    game_ip = None
    game_port = None

    while game_ip is None or game_port is None:
        print("=== Trying to get ip and port of a game ...")
        for key, game_detail in shared_game_ips_ports.items():
            if game_detail["taken"] == False and game_detail["ip"] is not "":
                game_ip = game_detail["ip"]
                game_port = game_detail["port"]
                updated_game_details = shared_game_ips_ports[key]
                updated_game_details["taken"] = True
                shared_game_ips_ports[key] = updated_game_details
                break

        time.sleep(1)
    
    if game_ip is None or game_port is None:
        raise ValueError(
            "Could not give port to a game client asking for it")
    print("=== Conneting to game at {}:{}".format(game_ip, game_port))
    game = GameCommunication(game_ip, game_port)

    try:
        conn.send((_READY, None))  # Send ready signal to AIGame-class for it to finish __init__.
        while True:
            try:
                # Only block for short times to have keyboard exceptions be raised.
                if not conn.poll(0.1):
                    continue
                message, payload = conn.recv()
            except (EOFError, KeyboardInterrupt):
                break
            if message == _GET_NUMBER_OF_OBSERVATION:
                assert payload is None
                result = game.get_observation_vector_length()
                conn.send((_RESULT, result))
                continue
            if message == _RESET:
                result = game.reset_game(payload)
                conn.send((_RESULT, result))
                continue
            if message == _IS_EPISODE_FINISHED:
                assert payload is None
                result = game.is_episode_finished()
                conn.send((_RESULT, result))
                continue
            if message == _MAKE_ACTION:
                assert payload is not None
                result = game.make_action(payload)
                conn.send((_RESULT, result))
                continue
            if message == _GET_OBSERVATIONS:
                assert payload is None
                result = game.get_observation_vector()
                conn.send((_RESULT, result))
                continue
            if message == _CLOSE:
                assert payload is None
                game.close_game()
                # conn.send((_RESULT, result))
                break
            raise KeyError('Received message of unknown type {}'.format(message))
    except Exception:  # pylint: disable=broad-except
        etype, evalue, tb = sys.exc_info()
        stacktrace = ''.join(traceback.format_exception(etype, evalue, tb))
        message = 'Error in environment process: {}'.format(stacktrace)
        logging.error(message)
        conn.send((_EXCEPTION, stacktrace))
    finally:
        conn.close()


def create_game_connections(num_parallel_training_environments, shared_game_ips_ports):
    connections = []
    for _ in range(num_parallel_training_environments):
        parent_conn, child_conn = Pipe()
        p = Process(target=start_clients, args=(child_conn, shared_game_ips_ports))
        p.start()

        connections.append(parent_conn)
    return connections