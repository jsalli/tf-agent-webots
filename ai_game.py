import os
import numpy as np
import time
import atexit
from subprocess import Popen, PIPE, STDOUT
import json


_READY = 1
_GET_NUMBER_OF_OBSERVATION = 2
_RESET = 3
_IS_EPISODE_FINISHED = 4
_MAKE_ACTION = 5
_GET_OBSERVATIONS = 6
_CLOSE = 7
_RESULT = 8
_EXCEPTION = 9


class AIGame:
    def __init__(self, connection, local_game, game_params_dict):
        self._conn = connection
        atexit.register(self.close)
        if local_game is True:
            self._process = self._start_game_process(
                game_params_dict["local"]["pathToExecutable"],
                game_params_dict["local"]["startScene"])

        print("#### Waiting for connection to send reset")
        self._conn.recv()
        
    def _start_game_process(self, game_executable_path, game_level_path):
        process = Popen(
            # [game_executable_path, "--batch", "--minimize", "--mode=fast", "--stderr", "--stdout", game_level_path],
            [game_executable_path, "--batch", "--mode=fast", game_level_path],
            stdout=None,
            stdin=PIPE,
            stderr=STDOUT,
            )
        print("=== Starting local game ===")
        return process


    def _receive(self):
        """Wait for a message from the worker process and return its payload.
        Raises:
        Exception: An exception was raised inside the worker process.
        KeyError: The reveived message is of an unknown type.
        Returns:
        Payload object of the message.
        """
        message, payload = self._conn.recv()
        # Re-raise exceptions in the main process.
        if message == _EXCEPTION:
            stacktrace = payload
            raise Exception(stacktrace)
        if message == _RESULT:
            return payload
        self.close()
        raise KeyError('Received message of unexpected type {}'.format(message))

    def close(self):
        if self._conn is not None:
            self._conn.send((_CLOSE, None))
        if self._process is not None:
            self._process.terminate()

    def get_num_of_actions(self):
        return self._number_of_actions
    

    def get_num_of_agents(self):
        return self._number_of_agents


    def get_num_of_observations(self):
        self._conn.send((_GET_NUMBER_OF_OBSERVATION, None))
        return self._receive()


    def reset(self, env_reset_parameters=None):
        if env_reset_parameters is not None:
            env_reset_parameters = json.dumps(env_reset_parameters)
        else:
            env_reset_parameters = ""
        self._conn.send((_RESET, env_reset_parameters))
        result = self._receive()
        self._number_of_actions = result[0]
        self._number_of_agents = result[1]
        return self._number_of_agents


    def is_episode_finished(self):
        self._conn.send((_IS_EPISODE_FINISHED, None))
        return self._receive()


    def make_action(self, action):
        self._conn.send((_MAKE_ACTION, action))
        return self._receive()
    

    def get_observations(self):
        self._conn.send((_GET_OBSERVATIONS, None))
        return self._receive()
