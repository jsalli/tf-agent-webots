import grpc
from multiprocessing import Process, Pipe

import game_communication.gamecommunication_single_pb2 as gcs_pb2
import game_communication.gamecommunication_single_pb2_grpc as gcs_pb2_grpc
 

class GameCommunication(object):
    def __init__(self, host, port):
        print("GameCommunication connected to game at: {}:{}".format(host, port))
        self.host = host
        self.server_port = port
        self._channel = grpc.insecure_channel('{}:{}'.format(self.host, self.server_port))
        self._stub = gcs_pb2_grpc.GameCommunicationStub(self._channel)


    def get_observation_vector(self):
        no_params = gcs_pb2.NoParams()
        response = self._stub.GetObservationVector(no_params)
        result_list = []
        for i in response.observations:
            result_list.append(i)
        return result_list


    def get_observation_vector_length(self):
        no_params = gcs_pb2.NoParams()
        response = self._stub.GetObservationVectorLength(no_params)
        return response.observationVectorLength


    def make_action(self, action):
        request = gcs_pb2.Action(action=action)
        response = self._stub.MakeAction(request)
        return response.reward


    def is_episode_finished(self):
        no_params = gcs_pb2.NoParams()
        response = self._stub.IsEpisodeFinished(no_params)
        return response.episodeFinished


    def close_game(self):
        no_params = gcs_pb2.NoParams()
        response = self._stub.CloseGame(no_params)
        return response.done


    def reset_game(self, game_level_params=""):
        game_params = gcs_pb2.GameParams(gameParamsJson=game_level_params)
        try:
            response = self._stub.ResetGame(game_params)
            return response.numberOfActions, response.numberOfAgents
        except Exception as e:
            print("==== GOT ERROR ")
            print(e)
            print("==== GOT ERROR ")
            raise ValueError("reset_game game error")


# For testing game_client
def test_run(_):
    game = GameCommunication("192.168.10.56", FLAGS.port)
    if FLAGS.action is None:
        reset_params = {
            "agent_max_steps": 5
            }
        reset_params_son = json.dumps(reset_params)
        print("=== reset_game")
        print(game.reset_game(reset_params_son))
        print("=== agent observation length")
        print(game.get_observation_vector_length())
        print("=== agent observation vector")
        print(game.get_observation_vector())
        print("=== make action")
        print(game.make_action(1))
        print("=== is_episode_finished")
        print(game.is_episode_finished())
        # print("=== close_game")
        # print(game.close_game())
    elif FLAGS.action is not None:
        print("=== make action")
        print(game.make_action(int(FLAGS.action)))
        print("=== agent observation vector")
        print(game.get_observation_vector())
        print("=== is_episode_finished")
        print(game.is_episode_finished())


# For testing game_client
if __name__ == '__main__':
    from absl import app
    from absl import flags
    import os
    import json
    import random
    flags.DEFINE_string('port', os.getenv('TEST_UNDECLARED_OUTPUTS_DIR'), 'Test port needed')
    flags.DEFINE_string('action', os.getenv('TEST_UNDECLARED_OUTPUTS_DIR'), 'Test action needed')

    FLAGS = flags.FLAGS
    flags.mark_flag_as_required('port')
    app.run(test_run)
