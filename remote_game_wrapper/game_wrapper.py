import os
import time
import json
import signal
import threading
from absl import app
from absl import flags
from subprocess import Popen, PIPE, STDOUT


flags.DEFINE_string("params_file_path", "game-wrapper-params.json", "Path to the parameters file", short_name="p")
FLAGS = flags.FLAGS

processes = []


def parse_parameters_file(params_file_path):
    parameters = None
    with open(params_file_path, 'r') as file:
        json_string = file.read()
        parameters = json.loads(json_string)
    return parameters


def close_games():
    for process in processes:
        process.terminate() 

def _start_game_process(game_executable_path, game_level_path):
    process = Popen(
        # [game_executable_path, "--batch", "--minimize", "--mode=fast", "--stderr", "--stdout", game_level_path],
        [game_executable_path, "--batch", "--mode=fast", game_level_path],
        stdout=None,
        stdin=PIPE,
        stderr=STDOUT)
    print("=== Starting local game ===")
    return process


def start(_):
    global process
    params_file_path = FLAGS.params_file_path
    game_params_dict = parse_parameters_file(params_file_path)

    number_of_games = game_params_dict["numberOfGames"]
    for _ in range(number_of_games):
        process =_start_game_process(
            game_params_dict["pathToExecutable"],
            game_params_dict["pathToStartLevel"])
        processes.append(process)

    print("All {} processes started".format(number_of_games))
    # Loop forever or until KeyboardInterrupt
    try:
        while True:
            time.sleep(60*60*60)
    except KeyboardInterrupt:
        print('Stopping processes ...')
        close_games()


if __name__ == '__main__':
    print("=== Starting main")
    flags.mark_flag_as_required('params_file_path')
    app.run(start)